package grpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
	pb "gitlab.com/Hugrid-1/cct-auth-microservice/rpc/grpc/protobuf"
)

type AuthGRPCServer struct {
	auth interfaces.Auther
	pb.UnimplementedAuthServer
}

func NewAuthGRPCServer(auth interfaces.Auther) *AuthGRPCServer {
	return &AuthGRPCServer{auth: auth}
}

func (a AuthGRPCServer) Register(ctx context.Context, in *pb.RegisterRequest) (*pb.RegisterResponse, error) {
	out := a.auth.Register(ctx, interfaces.RegisterIn{
		Email:          in.Email,
		Phone:          in.Phone,
		Password:       in.Password,
		IdempotencyKey: in.IdempotencyKey,
	})
	return &pb.RegisterResponse{
		Status:    int32(out.Status),
		ErrorCode: int32(out.ErrorCode),
	}, nil
}

func (a AuthGRPCServer) AuthorizeEmail(ctx context.Context, in *pb.AuthorizeEmailRequest) (*pb.AuthorizeResponse, error) {
	out := a.auth.AuthorizeEmail(ctx, interfaces.AuthorizeEmailIn{
		Email:          in.Email,
		Password:       in.Password,
		RetypePassword: in.RetypePassword,
	})
	return &pb.AuthorizeResponse{
		UserID:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a AuthGRPCServer) AuthorizeRefresh(ctx context.Context, in *pb.AuthorizeRefreshRequest) (*pb.AuthorizeResponse, error) {
	out := a.auth.AuthorizeRefresh(ctx, interfaces.AuthorizeRefreshIn{
		UserID: int(in.UserID),
	})
	return &pb.AuthorizeResponse{
		UserID:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a AuthGRPCServer) AuthorizePhone(ctx context.Context, in *pb.AuthorizePhoneRequest) (*pb.AuthorizeResponse, error) {
	out := a.auth.AuthorizePhone(ctx, interfaces.AuthorizePhoneIn{
		Phone: in.Phone,
		Code:  int(in.Code),
	})
	return &pb.AuthorizeResponse{
		UserID:       int32(out.UserID),
		AccessToken:  out.AccessToken,
		RefreshToken: out.RefreshToken,
		ErrorCode:    int32(out.ErrorCode),
	}, nil
}

func (a AuthGRPCServer) SendPhoneCode(ctx context.Context, in *pb.SendPhoneCodeRequest) (*pb.SendPhoneCodeResponse, error) {
	out := a.auth.SendPhoneCode(ctx, interfaces.SendPhoneCodeIn{
		Phone: in.Phone,
	})
	return &pb.SendPhoneCodeResponse{
		Phone: out.Phone,
		Code:  int32(out.Code),
	}, nil
}

func (a AuthGRPCServer) VerifyEmail(ctx context.Context, in *pb.VerifyEmailRequest) (*pb.VerifyEmailResponse, error) {
	out := a.auth.VerifyEmail(ctx, interfaces.VerifyEmailIn{
		Hash:  in.Hash,
		Email: in.Email,
	})
	return &pb.VerifyEmailResponse{
		Success:   out.Success,
		ErrorCode: int32(out.ErrorCode),
	}, nil
}
