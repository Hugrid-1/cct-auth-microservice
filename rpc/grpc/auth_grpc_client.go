package grpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
	pb "gitlab.com/Hugrid-1/cct-auth-microservice/rpc/grpc/protobuf"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type AuthGRPCClient struct {
	client pb.AuthClient
}

func NewAuthGRPCClient(address string) (*AuthGRPCClient, error) {
	conn, err := grpc.Dial(address, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		return nil, err
	}

	client := pb.NewAuthClient(conn)
	return &AuthGRPCClient{client: client}, nil
}

func (a AuthGRPCClient) Register(ctx context.Context, in interfaces.RegisterIn) interfaces.RegisterOut {
	request := &pb.RegisterRequest{
		Email:          in.Email,
		Phone:          in.Phone,
		Password:       in.Password,
		IdempotencyKey: in.IdempotencyKey,
	}
	response, err := a.client.Register(ctx, request)
	if err != nil {
		return interfaces.RegisterOut{ErrorCode: int(response.ErrorCode)}
	}

	return interfaces.RegisterOut{
		Status:    int(response.Status),
		ErrorCode: int(response.ErrorCode),
	}
}

func (a AuthGRPCClient) AuthorizeEmail(ctx context.Context, in interfaces.AuthorizeEmailIn) interfaces.AuthorizeOut {
	request := &pb.AuthorizeEmailRequest{
		Email:          in.Email,
		Password:       in.Password,
		RetypePassword: in.RetypePassword,
	}
	response, err := a.client.AuthorizeEmail(ctx, request)
	if err != nil {
		return interfaces.AuthorizeOut{
			ErrorCode: int(response.ErrorCode),
		}
	}

	return interfaces.AuthorizeOut{
		UserID:       int(response.UserID),
		AccessToken:  response.AccessToken,
		RefreshToken: response.RefreshToken,
		ErrorCode:    int(response.ErrorCode),
	}
}

func (a AuthGRPCClient) AuthorizeRefresh(ctx context.Context, in interfaces.AuthorizeRefreshIn) interfaces.AuthorizeOut {
	request := &pb.AuthorizeRefreshRequest{
		UserID: int32(in.UserID),
	}
	response, err := a.client.AuthorizeRefresh(ctx, request)
	if err != nil {
		return interfaces.AuthorizeOut{
			ErrorCode: int(response.ErrorCode),
		}
	}

	return interfaces.AuthorizeOut{
		UserID:       int(response.UserID),
		AccessToken:  response.AccessToken,
		RefreshToken: response.RefreshToken,
		ErrorCode:    int(response.ErrorCode),
	}
}

func (a AuthGRPCClient) AuthorizePhone(ctx context.Context, in interfaces.AuthorizePhoneIn) interfaces.AuthorizeOut {
	request := &pb.AuthorizePhoneRequest{
		Phone: in.Phone,
		Code:  int32(in.Code),
	}
	response, err := a.client.AuthorizePhone(ctx, request)
	if err != nil {
		return interfaces.AuthorizeOut{
			ErrorCode: int(response.ErrorCode),
		}
	}

	return interfaces.AuthorizeOut{
		UserID:       int(response.UserID),
		AccessToken:  response.AccessToken,
		RefreshToken: response.RefreshToken,
		ErrorCode:    int(response.ErrorCode),
	}
}

func (a AuthGRPCClient) SendPhoneCode(ctx context.Context, in interfaces.SendPhoneCodeIn) interfaces.SendPhoneCodeOut {
	request := &pb.SendPhoneCodeRequest{
		Phone: in.Phone,
	}
	response, err := a.client.SendPhoneCode(ctx, request)
	if err != nil {
		return interfaces.SendPhoneCodeOut{}
	}
	return interfaces.SendPhoneCodeOut{
		Phone: response.Phone,
		Code:  int(response.Code),
	}
}

func (a AuthGRPCClient) VerifyEmail(ctx context.Context, in interfaces.VerifyEmailIn) interfaces.VerifyEmailOut {
	request := &pb.VerifyEmailRequest{
		Hash:  in.Hash,
		Email: in.Email,
	}
	response, err := a.client.VerifyEmail(ctx, request)
	if err != nil {
		return interfaces.VerifyEmailOut{
			ErrorCode: int(response.ErrorCode),
		}
	}
	return interfaces.VerifyEmailOut{
		Success:   response.Success,
		ErrorCode: int(response.ErrorCode),
	}
}
