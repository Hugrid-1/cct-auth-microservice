package jrpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
)

// AuthJSONRPCServer представляет Auth для использования в JSON-RPC
type AuthJRPCServer struct {
	auth interfaces.Auther
}

// NewAuthJSONRPCServer возвращает новый AuthJSONRPCServer
func NewAuthJSONRPCServer(Auth interfaces.Auther) *AuthJRPCServer {
	return &AuthJRPCServer{auth: Auth}
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод Register из Auth.
// Возвращает выходные данные.
func (s *AuthJRPCServer) Register(ctx context.Context, in interfaces.RegisterIn) interfaces.RegisterOut {
	out := s.auth.Register(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizeEmail из Auth.
// Возвращает выходные данные.
func (s *AuthJRPCServer) AuthorizeEmail(ctx context.Context, in interfaces.AuthorizeEmailIn) interfaces.AuthorizeOut {
	out := s.auth.AuthorizeEmail(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizeRefresh из Auth.
// Возвращает выходные данные.
func (s *AuthJRPCServer) AuthorizeRefresh(ctx context.Context, in interfaces.AuthorizeRefreshIn) interfaces.AuthorizeOut {
	out := s.auth.AuthorizeRefresh(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод AuthorizePhone из Auth.
// Возвращает выходные данные.
func (s *AuthJRPCServer) AuthorizePhone(ctx context.Context, in interfaces.AuthorizePhoneIn) interfaces.AuthorizeOut {
	out := s.auth.AuthorizePhone(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод SendPhoneCode из Auth.
// Возвращает выходные данные.
func (s *AuthJRPCServer) SendPhoneCode(ctx context.Context, in interfaces.SendPhoneCodeIn) interfaces.SendPhoneCodeOut {
	out := s.auth.SendPhoneCode(ctx, in)
	return out
}

// Принимает входные данные, использует контекст по умолчанию и вызывает метод VerifyEmail из Auth.
// Возвращает выходные данные.
func (s *AuthJRPCServer) VerifyEmail(ctx context.Context, in interfaces.VerifyEmailIn) interfaces.VerifyEmailOut {
	out := s.auth.VerifyEmail(ctx, in)
	return out
}
