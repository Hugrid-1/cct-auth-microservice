package jrpc

import (
	"context"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
	"log"
	"net/rpc"
)

type AuthJRPCClient struct {
	client *rpc.Client
}

func NewAuthClient(serverAddress string) (*AuthJRPCClient, error) {
	client, err := rpc.Dial("tcp", serverAddress)
	if err != nil {
		return nil, err
	}

	return &AuthJRPCClient{
		client: client,
	}, nil
}

func (c *AuthJRPCClient) Register(ctx context.Context, in interfaces.RegisterIn) interfaces.RegisterOut {
	var out interfaces.RegisterOut
	err := c.client.Call("Auther.Register", in, &out)
	if err != nil {
		log.Fatal("Ошибка при вызове метода Register:", err)
	}
	return out
}

func (c *AuthJRPCClient) AuthorizeEmail(ctx context.Context, in interfaces.AuthorizeEmailIn) interfaces.AuthorizeOut {
	var out interfaces.AuthorizeOut
	err := c.client.Call("Auther.AuthorizeEmail", in, &out)
	if err != nil {
		log.Fatal("Ошибка при вызове метода AuthorizeEmail:", err)
	}
	return out
}

func (c *AuthJRPCClient) AuthorizeRefresh(ctx context.Context, in interfaces.AuthorizeRefreshIn) interfaces.AuthorizeOut {
	var out interfaces.AuthorizeOut
	err := c.client.Call("Auther.AuthorizeRefresh", in, &out)
	if err != nil {
		log.Fatal("Ошибка при вызове метода AuthorizeRefresh:", err)
	}
	return out
}

func (c *AuthJRPCClient) AuthorizePhone(ctx context.Context, in interfaces.AuthorizePhoneIn) interfaces.AuthorizeOut {
	var out interfaces.AuthorizeOut
	err := c.client.Call("Auther.AuthorizePhone", in, &out)
	if err != nil {
		log.Fatal("Ошибка при вызове метода AuthorizePhone:", err)
	}
	return out
}

func (c *AuthJRPCClient) SendPhoneCode(ctx context.Context, in interfaces.SendPhoneCodeIn) interfaces.SendPhoneCodeOut {
	var out interfaces.SendPhoneCodeOut
	err := c.client.Call("Auther.SendPhoneCode", in, &out)
	if err != nil {
		log.Fatal("Ошибка при вызове метода SendPhoneCode:", err)
	}
	return out
}

func (c *AuthJRPCClient) VerifyEmail(ctx context.Context, in interfaces.VerifyEmailIn) interfaces.VerifyEmailOut {
	var out interfaces.VerifyEmailOut
	err := c.client.Call("Auther.VerifyEmail", in, &out)
	if err != nil {
		log.Fatal("Ошибка при вызове метода VerifyEmail:", err)
	}
	return out
}
