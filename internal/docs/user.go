package docs

import (
	"gitlab.com/Hugrid-1/cct-user-microservice/public/messages"
)

//go:generate swagger generate spec -o ../../static/swagger.json --scan-models

// swagger:route GET /api/1/user/profile user profileRequest
// Получение информации о текущем пользователе.
// security:
//   - Bearer: []
// responses:
//   200: profileResponse

// swagger:response profileResponse
type profileResponse struct {
	// in:body
	Body messages.ProfileResponse
}

// swagger:route POST /api/1/user/profile/password_change user changePasswordRequest
// Изменение пароля текущего пользователя.
// security:
// 	-Bearer: []
// responses:
// 	200: changePasswordResponse

// swagger:parameters changePasswordRequest
type changePasswordRequest struct {
	// in: body
	Body messages.ChangePasswordRequest
}

// swagger:response changePasswordResponse
type changePasswordResponse struct {
	// in: body
	Body messages.ChangePasswordResponse
}
