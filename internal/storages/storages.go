package storages

import (
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/db/adapter"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/cache"
	vstorage "gitlab.com/Hugrid-1/cct-auth-microservice/internal/modules/auth/storage"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
)

type Storages struct {
	Verify interfaces.Verifier
}

func NewStorages(sqlAdapter *adapter.SQLAdapter, cache cache.Cache) *Storages {
	return &Storages{
		Verify: vstorage.NewEmailVerify(sqlAdapter),
	}
}
