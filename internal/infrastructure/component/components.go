package component

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Hugrid-1/cct-auth-microservice/config"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/responder"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/service"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/tools/cryptography"
	"go.uber.org/zap"
	"net/rpc"
)

type Components struct {
	Conf          config.AppConf
	Notify        service.Notifier
	TokenManager  cryptography.TokenManager
	Responder     responder.Responder
	Decoder       godecoder.Decoder
	Logger        *zap.Logger
	Hash          cryptography.Hasher
	UserRPCClient *rpc.Client
}

func NewComponents(conf config.AppConf, notify service.Notifier, tokenManager cryptography.TokenManager, responder responder.Responder, decoder godecoder.Decoder, hash cryptography.Hasher, logger *zap.Logger) *Components {
	return &Components{Conf: conf, Notify: notify, TokenManager: tokenManager, Responder: responder, Decoder: decoder, Hash: hash, Logger: logger}
}
