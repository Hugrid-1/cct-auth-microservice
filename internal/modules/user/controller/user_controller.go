package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/handlers"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/responder"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/messages"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
	"net/http"
)

type Userer interface {
	Profile(http.ResponseWriter, *http.Request)
	GetUsersInfo(http.ResponseWriter, *http.Request)
	ChangePassword(http.ResponseWriter, *http.Request)
}

type User struct {
	service user_interface.Userer
	responder.Responder
	godecoder.Decoder
}

func NewUser(service user_interface.Userer, components *component.Components) Userer {
	return &User{service: service, Responder: components.Responder, Decoder: components.Decoder}
}

func (u *User) Profile(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}
	out := u.service.GetByID(r.Context(), user_interface.GetByIDIn{UserID: claims.ID})
	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ProfileResponse{
			ErrorCode: out.ErrorCode,
			Data: messages.Data{
				Message: "retrieving user error",
			},
		})
		return
	}

	u.OutputJSON(w, messages.ProfileResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data: messages.Data{
			User: *out.User,
		},
	})
}

func (u *User) GetUsersInfo(w http.ResponseWriter, r *http.Request) {
	panic("implement me")
}

func (u *User) ChangePassword(w http.ResponseWriter, r *http.Request) {
	var changePasswordRequest messages.ChangePasswordRequest
	err := u.Decode(r.Body, &changePasswordRequest)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	claims, err := handlers.ExtractUser(r)
	if err != nil {
		u.ErrorBadRequest(w, err)
		return
	}

	out := u.service.ChangePassword(r.Context(), user_interface.ChangePasswordIn{
		UserID:      claims.ID,
		OldPassword: changePasswordRequest.OldPassword,
		NewPassword: changePasswordRequest.NewPassword,
	})

	if out.ErrorCode != errors.NoError {
		u.OutputJSON(w, messages.ChangePasswordResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data:      messages.Data{Message: "password change error"},
		})
		return
	}

	u.OutputJSON(w, messages.ChangePasswordResponse{
		Success:   true,
		ErrorCode: out.ErrorCode,
		Data:      messages.Data{Message: "password successfully changed"},
	})

}
