package modules

import (
	"fmt"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/component"
	aservice "gitlab.com/Hugrid-1/cct-auth-microservice/internal/modules/auth/service"
	uservice "gitlab.com/Hugrid-1/cct-auth-microservice/internal/modules/user/service"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/storages"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"
	userGRPC "gitlab.com/Hugrid-1/cct-user-microservice/rpc/grpc"
	userJRPC "gitlab.com/Hugrid-1/cct-user-microservice/rpc/jrpc"
	"log"
)

type Services struct {
	User user_interface.Userer
	Auth interfaces.Auther
}

func NewServices(storages *storages.Storages, components *component.Components) *Services {
	var rpcService *uservice.UserServiceRPC
	userRPCAddress := fmt.Sprintf("%s:%s", components.Conf.UserRPC.Host, components.Conf.UserRPC.Port)
	switch components.Conf.RPCServer.Type {
	case "grpc":
		userClient, err := userGRPC.NewUserGRPCClient(userRPCAddress)
		if err != nil {
			log.Printf("Error creating user RPC client: %v", err)
		}
		rpcService = uservice.NewUserRPCService(userClient)
	case "jsonrpc":
		userClient, err := userJRPC.NewUserJRPCClient(userRPCAddress)
		if err != nil {
			log.Printf("Error creating user RPC client: %v", err)
		}
		rpcService = uservice.NewUserRPCService(userClient)
	default:
		components.Logger.Fatal("error: bad rpc type ( check user .env file )")
	}

	return &Services{
		User: rpcService,
		Auth: aservice.NewAuth(rpcService, storages.Verify, components),
	}
}
