package controller

import (
	"github.com/ptflp/godecoder"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/handlers"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/responder"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/messages"
	"net/http"
	"net/mail"
)

type Auther interface {
	Register(http.ResponseWriter, *http.Request)
	Login(http.ResponseWriter, *http.Request)
	Refresh(w http.ResponseWriter, r *http.Request)
	Verify(w http.ResponseWriter, r *http.Request)
}

type Auth struct {
	auth interfaces.Auther
	responder.Responder
	godecoder.Decoder
}

func NewAuth(service interfaces.Auther, components *component.Components) Auther {
	return &Auth{auth: service, Responder: components.Responder, Decoder: components.Decoder}
}

func valid(email string) bool {
	_, err := mail.ParseAddress(email)
	return err == nil
}

func (a *Auth) Register(w http.ResponseWriter, r *http.Request) {
	var req messages.RegisterRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}

	if !valid(req.Email) {
		a.OutputJSON(w, messages.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.Data{
				Message: "invalid email",
			},
		})
		return
	}

	if req.Password != req.RetypePassword {
		a.OutputJSON(w, messages.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.Data{
				Message: "passwords mismatch",
			},
		})
		return
	}

	out := a.auth.Register(r.Context(), interfaces.RegisterIn{
		Email:    req.Email,
		Password: req.Password,
	})

	if out.ErrorCode != errors.NoError {
		msg := "register error"
		if out.ErrorCode == errors.UserServiceUserAlreadyExists {
			msg = "User already exists, please check your email"
		}
		a.OutputJSON(w, messages.RegisterResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.Data{
				Message: msg,
			},
		})
		return
	}

	a.OutputJSON(w, messages.RegisterResponse{
		Success: true,
		Data: messages.Data{
			Message: "verification link sent to " + req.Email,
		},
	})
}

func (a *Auth) Login(w http.ResponseWriter, r *http.Request) {
	var req messages.LoginRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, messages.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.Data{
				Message: "phone or email empty",
			},
		})
	}

	out := a.auth.AuthorizeEmail(r.Context(), interfaces.AuthorizeEmailIn{
		Email:    req.Email,
		Password: req.Password,
	})
	if out.ErrorCode == errors.AuthServiceUserNotVerified {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.LoginData{
				Message: "user email is not verified",
			},
		})
		return
	}

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, messages.AuthResponse{
		Success: true,
		Data: messages.LoginData{
			Message:      "success login",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

func (a *Auth) Refresh(w http.ResponseWriter, r *http.Request) {
	claims, err := handlers.ExtractUser(r)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	out := a.auth.AuthorizeRefresh(r.Context(), interfaces.AuthorizeRefreshIn{UserID: claims.ID})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, messages.AuthResponse{
		Success: true,
		Data: messages.LoginData{
			Message:      "success refresh",
			AccessToken:  out.AccessToken,
			RefreshToken: out.RefreshToken,
		},
	})
}

func (a *Auth) Verify(w http.ResponseWriter, r *http.Request) {
	var req messages.VerifyRequest
	err := a.Decode(r.Body, &req)
	if err != nil {
		a.ErrorBadRequest(w, err)
		return
	}
	if len(req.Email) < 5 {
		a.OutputJSON(w, messages.RegisterResponse{
			Success:   false,
			ErrorCode: http.StatusBadRequest,
			Data: messages.Data{
				Message: "phone or email empty",
			},
		})
	}

	out := a.auth.VerifyEmail(r.Context(), interfaces.VerifyEmailIn{
		Email: req.Email,
		Hash:  req.Hash,
	})

	if out.ErrorCode != errors.NoError {
		a.OutputJSON(w, messages.AuthResponse{
			Success:   false,
			ErrorCode: out.ErrorCode,
			Data: messages.LoginData{
				Message: "login or password mismatch",
			},
		})
		return
	}

	a.OutputJSON(w, messages.AuthResponse{
		Success: true,
		Data: messages.LoginData{
			Message: "email verification success",
		},
	})
}
