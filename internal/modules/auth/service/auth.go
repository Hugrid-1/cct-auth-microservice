package service

import (
	"context"
	"gitlab.com/Hugrid-1/cct-auth-microservice/config"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/errors"
	iservice "gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/service"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/tools/cryptography"
	"gitlab.com/Hugrid-1/cct-auth-microservice/public/interfaces"
	user_models "gitlab.com/Hugrid-1/cct-user-microservice/public/models"
	"gitlab.com/Hugrid-1/cct-user-microservice/public/user_interface"

	"go.uber.org/zap"
	"net/http"
	"net/url"
	"strconv"
	"time"
)

type Auth struct {
	conf         config.AppConf
	user         user_interface.Userer
	verify       interfaces.Verifier
	notify       iservice.Notifier
	tokenManager cryptography.TokenManager
	hash         cryptography.Hasher
	logger       *zap.Logger
}

func NewAuth(user user_interface.Userer, verify interfaces.Verifier, components *component.Components) *Auth {
	return &Auth{conf: components.Conf,
		user:         user,
		verify:       verify,
		notify:       components.Notify,
		tokenManager: components.TokenManager,
		hash:         components.Hash,
		logger:       components.Logger,
	}
}

func (a *Auth) SetUserer(user user_interface.Userer) {
	a.user = user
}

func (a *Auth) Register(ctx context.Context, in interfaces.RegisterIn) interfaces.RegisterOut {
	hashPass, err := cryptography.HashPassword(in.Password)
	if err != nil {
		return interfaces.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: errors.HashPasswordError,
		}
	}

	userCreate := user_interface.UserCreateIn{
		Email:    in.Email,
		Password: hashPass,
	}

	userOut := a.user.Create(ctx, userCreate)
	if userOut.ErrorCode != errors.NoError {
		if userOut.ErrorCode == errors.UserServiceUserAlreadyExists {
			return interfaces.RegisterOut{
				Status:    http.StatusConflict,
				ErrorCode: userOut.ErrorCode,
			}
		}
		return interfaces.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := a.user.GetByEmail(ctx, user_interface.GetByEmailIn{Email: in.Email})
	if user.ErrorCode != errors.NoError {
		return interfaces.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: userOut.ErrorCode,
		}
	}

	hash := a.hash.GenHashString(nil, cryptography.UUID)
	err = a.verify.Create(ctx, in.Email, hash, user.User.ID)
	if err != nil {
		return interfaces.RegisterOut{
			Status:    http.StatusInternalServerError,
			ErrorCode: http.StatusInternalServerError,
		}
	}
	go a.sendEmailVerifyLink(in.Email, hash)

	return interfaces.RegisterOut{
		Status:    http.StatusOK,
		ErrorCode: errors.NoError,
	}
}

func (a *Auth) sendEmailVerifyLink(email, hash string) int {
	userOut := a.user.GetByEmail(context.Background(), user_interface.GetByEmailIn{Email: email})
	if userOut.ErrorCode != errors.NoError {
		return userOut.ErrorCode
	}

	u, err := url.Parse("http://bing.com/verify?email=sample&hash=sample")
	if err != nil {
		a.logger.Fatal("auth: url parse err", zap.Error(err))

		return errors.AuthUrlParseErr
	}
	u.Scheme = "https"
	if a.conf.Environment != "production" {
		u.Scheme = "http"
	}
	u.Host = a.conf.Domain
	q := u.Query()
	q.Set("email", email)
	q.Set("hash", hash)
	u.RawQuery = q.Encode()
	a.notifyEmail(iservice.PushIn{
		Identifier: email,
		Type:       iservice.PushEmail,
		Title:      "Activation Link",
		Data:       []byte(u.String()),
		Options:    nil,
	})

	return errors.NoError
}

// TODO: Refactor
func (a *Auth) notifyEmail(p iservice.PushIn) {
	res := a.notify.Push(p)
	if res.ErrorCode != errors.NoError {
		time.Sleep(1 * time.Minute)
		go a.notifyEmail(p)
	}
}

func (a *Auth) AuthorizeEmail(ctx context.Context, in interfaces.AuthorizeEmailIn) interfaces.AuthorizeOut {
	userOut := a.user.GetByEmail(ctx, user_interface.GetByEmailIn{Email: in.Email})
	if userOut.ErrorCode != errors.NoError {
		return interfaces.AuthorizeOut{
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := userOut.User
	if !cryptography.CheckPassword(user.Password, in.Password) {
		return interfaces.AuthorizeOut{
			ErrorCode: errors.AuthServiceWrongPasswordErr,
		}
	}
	if !user.EmailVerified {
		return interfaces.AuthorizeOut{
			ErrorCode: errors.AuthServiceUserNotVerified,
		}
	}

	accessToken, refreshToken, errorCode := a.generateTokens(user)
	if errorCode != errors.NoError {
		return interfaces.AuthorizeOut{
			ErrorCode: errorCode,
		}
	}

	return interfaces.AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) AuthorizeRefresh(ctx context.Context, in interfaces.AuthorizeRefreshIn) interfaces.AuthorizeOut {
	userOut := a.user.GetByID(ctx, user_interface.GetByIDIn{UserID: in.UserID})
	if userOut.ErrorCode != errors.NoError {
		return interfaces.AuthorizeOut{
			ErrorCode: userOut.ErrorCode,
		}
	}
	user := userOut.User

	accessToken, refreshToken, errorCode := a.generateTokens(user)
	if errorCode != errors.NoError {
		return interfaces.AuthorizeOut{
			ErrorCode: errorCode,
		}
	}

	return interfaces.AuthorizeOut{
		UserID:       user.ID,
		AccessToken:  accessToken,
		RefreshToken: refreshToken,
	}
}

func (a *Auth) generateTokens(user *user_models.User) (string, string, int) {
	accessToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.AccessTTL,
		cryptography.AccessToken,
	)
	if err != nil {
		a.logger.Error("auth: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceAccessTokenGenerationErr
	}
	refreshToken, err := a.tokenManager.CreateToken(
		strconv.Itoa(user.ID),
		strconv.Itoa(user.Role),
		"",
		a.conf.Token.RefreshTTL,
		cryptography.RefreshToken,
	)
	if err != nil {
		a.logger.Error("auth: create access token err", zap.Error(err))
		return "", "", errors.AuthServiceRefreshTokenGenerationErr
	}

	return accessToken, refreshToken, errors.NoError
}

func (a *Auth) AuthorizePhone(ctx context.Context, in interfaces.AuthorizePhoneIn) interfaces.AuthorizeOut {
	return interfaces.AuthorizeOut{}
}

func (a *Auth) SendPhoneCode(ctx context.Context, in interfaces.SendPhoneCodeIn) interfaces.SendPhoneCodeOut {
	panic("asfasf")
}

func (a *Auth) VerifyEmail(ctx context.Context, in interfaces.VerifyEmailIn) interfaces.VerifyEmailOut {
	dto, err := a.verify.GetByEmail(ctx, in.Email, in.Hash)
	if err != nil {
		return interfaces.VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	err = a.verify.VerifyEmail(ctx, dto.GetEmail(), dto.GetHash())
	if err != nil {
		return interfaces.VerifyEmailOut{
			ErrorCode: errors.AuthServiceVerifyErr,
		}
	}
	out := a.user.VerifyEmail(ctx, user_interface.UserVerifyEmailIn{
		UserID: dto.GetUserID(),
	})

	return interfaces.VerifyEmailOut{
		Success:   out.Success,
		ErrorCode: out.ErrorCode,
	}
}
