package run

import (
	"context"
	"fmt"
	"github.com/go-chi/chi/v5"
	jsoniter "github.com/json-iterator/go"
	"github.com/ptflp/godecoder"
	"gitlab.com/Hugrid-1/cct-auth-microservice/config"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/db"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/cache"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/component"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/db/migrate"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/db/scanner"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/errors"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/responder"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/router"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/server"
	internal "gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/service"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/infrastructure/tools/cryptography"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/modules"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/provider"
	"gitlab.com/Hugrid-1/cct-auth-microservice/internal/storages"
	models2 "gitlab.com/Hugrid-1/cct-auth-microservice/public/models"
	grpc2 "gitlab.com/Hugrid-1/cct-auth-microservice/rpc/grpc"
	pb "gitlab.com/Hugrid-1/cct-auth-microservice/rpc/grpc/protobuf"
	"gitlab.com/Hugrid-1/cct-auth-microservice/rpc/jrpc"
	"go.uber.org/zap"
	"golang.org/x/sync/errgroup"
	"google.golang.org/grpc"
	"google.golang.org/grpc/reflection"
	"net/http"
	"net/rpc"
	"os"
)

// Application - интерфейс приложения
type Application interface {
	Runner
	Bootstraper
}

// Runner - интерфейс запуска приложения
type Runner interface {
	Run() int
}

// Bootstraper - интерфейс инициализации приложения
type Bootstraper interface {
	Bootstrap(options ...interface{}) Runner
}

// App - структура приложения
type App struct {
	conf      config.AppConf
	logger    *zap.Logger
	srv       server.Server
	RPCServer server.Server
	Sig       chan os.Signal
	Storages  *storages.Storages
	Servises  *modules.Services
}

// NewApp - конструктор приложения
func NewApp(conf config.AppConf, logger *zap.Logger) *App {
	return &App{conf: conf, logger: logger, Sig: make(chan os.Signal, 1)}
}

// Run - запуск приложения
func (a *App) Run() int {
	// на русском
	// создаем контекст для graceful shutdown
	ctx, cancel := context.WithCancel(context.Background())

	errGroup, ctx := errgroup.WithContext(ctx)

	// запускаем горутину для graceful shutdown
	// при получении сигнала SIGINT
	// вызываем cancel для контекста
	errGroup.Go(func() error {
		sigInt := <-a.Sig
		a.logger.Info("signal interrupt recieved", zap.Stringer("os_signal", sigInt))
		cancel()
		return nil
	})

	// запускаем http сервер
	errGroup.Go(func() error {
		err := a.srv.Serve(ctx)
		if err != nil && err != http.ErrServerClosed {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	// запускаем  rpc сервер
	errGroup.Go(func() error {
		err := a.RPCServer.Serve(ctx)
		if err != nil {
			a.logger.Error("app: server error", zap.Error(err))
			return err
		}
		return nil
	})

	if err := errGroup.Wait(); err != nil {
		return errors.GeneralError
	}

	return errors.NoError
}

// Bootstrap - инициализация приложения
func (a *App) Bootstrap(options ...interface{}) Runner {
	// на русском
	// инициализация емейл провайдера
	email := provider.NewEmail(a.conf.Provider.Email, a.logger)
	// инициализация сервиса нотификации
	notifyEmail := internal.NewNotify(a.conf.Provider.Email, email, a.logger)
	// инициализация менеджера токенов
	tokenManager := cryptography.NewTokenJWT(a.conf.Token)
	// инициализация декодера
	decoder := godecoder.NewDecoder(jsoniter.Config{
		EscapeHTML:             true,
		SortMapKeys:            true,
		ValidateJsonRawMessage: true,
		DisallowUnknownFields:  true,
	})
	// инициализация менеджера ответов сервера
	responseManager := responder.NewResponder(decoder, a.logger)
	// инициализация генератора uuid
	uuID := cryptography.NewUUIDGenerator()
	// инициализация хешера
	hash := cryptography.NewHash(uuID)
	// инициализация компонентов
	components := component.NewComponents(a.conf, notifyEmail, tokenManager, responseManager, decoder, hash, a.logger)
	// инициализация сканера таблиц
	tableScanner := scanner.NewTableScanner()
	// регистрация таблиц
	tableScanner.RegisterTable(
		&models2.EmailVerifyDTO{},
	)
	// инициализация базы данных sql и его адаптера
	dbx, sqlAdapter, err := db.NewSqlDB(a.conf.DB, tableScanner, a.logger)
	if err != nil {
		a.logger.Fatal("error init db", zap.Error(err))
	}
	// инициализация мигратора
	migrator := migrate.NewMigrator(dbx, a.conf.DB, tableScanner)
	err = migrator.Migrate()
	if err != nil {
		a.logger.Fatal("migrator err", zap.Error(err))
	}
	// инициализация кэша основанного на redis
	cacheClient, err := cache.NewCache(a.conf.Cache, decoder, a.logger)
	if err != nil {
		a.logger.Fatal("error init cache", zap.Error(err))
	}

	// инициализация хранилищ
	newStorages := storages.NewStorages(sqlAdapter, cacheClient)
	a.Storages = newStorages
	// инициализация сервисов
	services := modules.NewServices(newStorages, components)
	a.Servises = services

	switch a.conf.RPCServer.Type {
	case "grpc":
		authRPC := grpc2.NewAuthGRPCServer(services.Auth)
		gRPCServer := grpc.NewServer()
		reflection.Register(gRPCServer)
		pb.RegisterAuthServer(gRPCServer, authRPC)
		a.RPCServer = server.NewGRPCServer(a.conf.RPCServer, gRPCServer, a.logger)
	case "jsonrpc":
		// инициализация сервиса User в json RPC
		authRPC := jrpc.NewAuthJSONRPCServer(services.Auth)
		jsonRPCServer := rpc.NewServer()
		err = jsonRPCServer.Register(authRPC)
		if err != nil {
			a.logger.Fatal("error: User JRPC register failed")
		}
		// инициализация сервера json RPC
		a.RPCServer = server.NewJSONRPC(a.conf.RPCServer, jsonRPCServer, a.logger)
	default:
		a.logger.Fatal("error: bad rpc type ( check user .env file )")
	}

	controllers := modules.NewControllers(services, components)
	// инициализация роутера
	var r *chi.Mux
	r = router.NewRouter(controllers, components)
	// конфигурация сервера
	srv := &http.Server{
		Addr:    fmt.Sprintf(":%s", a.conf.Server.Port),
		Handler: r,
	}
	// инициализация сервера
	a.srv = server.NewHttpServer(a.conf.Server, srv, a.logger)
	// возвращаем приложение
	return a
}
